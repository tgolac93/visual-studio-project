﻿namespace Emergency
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.OutputTable = new System.Windows.Forms.DataGridView();
            this.labelTitleEmergencyServices = new System.Windows.Forms.Label();
            this.ResetButton = new System.Windows.Forms.Button();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.textBoxOrgId = new System.Windows.Forms.TextBox();
            this.groupBoxSearch = new System.Windows.Forms.GroupBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelOrgId = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.OrgNameText = new System.Windows.Forms.TextBox();
            this.labelOrgName = new System.Windows.Forms.Label();
            this.CountyCombo = new System.Windows.Forms.ComboBox();
            this.labelCounty = new System.Windows.Forms.Label();
            this.StateCombo = new System.Windows.Forms.ComboBox();
            this.labelState = new System.Windows.Forms.Label();
            this.TownCombo = new System.Windows.Forms.ComboBox();
            this.labelTown = new System.Windows.Forms.Label();
            this.OrgTypeCombo = new System.Windows.Forms.ComboBox();
            this.labelOrgType = new System.Windows.Forms.Label();
            this.labelInstructions = new System.Windows.Forms.Label();
            this.groupBoxInstructions = new System.Windows.Forms.GroupBox();
            this.QuitButton = new System.Windows.Forms.Button();
            this.BGSearch = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.OutputTable)).BeginInit();
            this.groupBoxSearch.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxInstructions.SuspendLayout();
            this.SuspendLayout();
            // 
            // OutputTable
            // 
            this.OutputTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.OutputTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.OutputTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OutputTable.Location = new System.Drawing.Point(12, 469);
            this.OutputTable.Margin = new System.Windows.Forms.Padding(4);
            this.OutputTable.Name = "OutputTable";
            this.OutputTable.ReadOnly = true;
            this.OutputTable.Size = new System.Drawing.Size(963, 358);
            this.OutputTable.TabIndex = 17;
            this.OutputTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OutputTable_CellContentClick);
            // 
            // labelTitleEmergencyServices
            // 
            this.labelTitleEmergencyServices.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitleEmergencyServices.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTitleEmergencyServices.Location = new System.Drawing.Point(91, -117);
            this.labelTitleEmergencyServices.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTitleEmergencyServices.Name = "labelTitleEmergencyServices";
            this.labelTitleEmergencyServices.Size = new System.Drawing.Size(400, 52);
            this.labelTitleEmergencyServices.TabIndex = 16;
            this.labelTitleEmergencyServices.Text = "Emergency Services";
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(250, 354);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(4);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(61, 34);
            this.ResetButton.TabIndex = 14;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.Location = new System.Drawing.Point(321, 37);
            this.buttonDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Size = new System.Drawing.Size(76, 28);
            this.buttonDisplay.TabIndex = 13;
            this.buttonDisplay.Text = "Display";
            this.buttonDisplay.UseVisualStyleBackColor = true;
            this.buttonDisplay.Click += new System.EventHandler(this.DisplayButton_Click);
            // 
            // textBoxOrgId
            // 
            this.textBoxOrgId.Location = new System.Drawing.Point(120, 39);
            this.textBoxOrgId.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOrgId.Name = "textBoxOrgId";
            this.textBoxOrgId.Size = new System.Drawing.Size(193, 22);
            this.textBoxOrgId.TabIndex = 12;
            // 
            // groupBoxSearch
            // 
            this.groupBoxSearch.Controls.Add(this.SaveButton);
            this.groupBoxSearch.Controls.Add(this.LoadButton);
            this.groupBoxSearch.Controls.Add(this.groupBox1);
            this.groupBoxSearch.Controls.Add(this.ResetButton);
            this.groupBoxSearch.Controls.Add(this.SearchButton);
            this.groupBoxSearch.Controls.Add(this.OrgNameText);
            this.groupBoxSearch.Controls.Add(this.labelOrgName);
            this.groupBoxSearch.Controls.Add(this.CountyCombo);
            this.groupBoxSearch.Controls.Add(this.labelCounty);
            this.groupBoxSearch.Controls.Add(this.StateCombo);
            this.groupBoxSearch.Controls.Add(this.labelState);
            this.groupBoxSearch.Controls.Add(this.TownCombo);
            this.groupBoxSearch.Controls.Add(this.labelTown);
            this.groupBoxSearch.Controls.Add(this.OrgTypeCombo);
            this.groupBoxSearch.Controls.Add(this.labelOrgType);
            this.groupBoxSearch.Location = new System.Drawing.Point(13, 52);
            this.groupBoxSearch.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxSearch.Name = "groupBoxSearch";
            this.groupBoxSearch.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxSearch.Size = new System.Drawing.Size(651, 396);
            this.groupBoxSearch.TabIndex = 15;
            this.groupBoxSearch.TabStop = false;
            this.groupBoxSearch.Text = "Search";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(459, 354);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(61, 35);
            this.SaveButton.TabIndex = 17;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(541, 353);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 35);
            this.LoadButton.TabIndex = 16;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxOrgId);
            this.groupBox1.Controls.Add(this.labelOrgId);
            this.groupBox1.Controls.Add(this.buttonDisplay);
            this.groupBox1.Location = new System.Drawing.Point(157, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 72);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search by ID";
            // 
            // labelOrgId
            // 
            this.labelOrgId.AutoSize = true;
            this.labelOrgId.Location = new System.Drawing.Point(6, 42);
            this.labelOrgId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOrgId.Name = "labelOrgId";
            this.labelOrgId.Size = new System.Drawing.Size(106, 17);
            this.labelOrgId.TabIndex = 11;
            this.labelOrgId.Text = "Organization ID";
            // 
            // SearchButton
            // 
            this.SearchButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SearchButton.Location = new System.Drawing.Point(157, 354);
            this.SearchButton.Margin = new System.Windows.Forms.Padding(4);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(70, 34);
            this.SearchButton.TabIndex = 10;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // OrgNameText
            // 
            this.OrgNameText.Location = new System.Drawing.Point(158, 161);
            this.OrgNameText.Margin = new System.Windows.Forms.Padding(4);
            this.OrgNameText.Name = "OrgNameText";
            this.OrgNameText.Size = new System.Drawing.Size(485, 22);
            this.OrgNameText.TabIndex = 9;
            // 
            // labelOrgName
            // 
            this.labelOrgName.AutoSize = true;
            this.labelOrgName.Location = new System.Drawing.Point(25, 165);
            this.labelOrgName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOrgName.Name = "labelOrgName";
            this.labelOrgName.Size = new System.Drawing.Size(130, 17);
            this.labelOrgName.TabIndex = 8;
            this.labelOrgName.Text = "Organization Name";
            // 
            // CountyCombo
            // 
            this.CountyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CountyCombo.Enabled = false;
            this.CountyCombo.FormattingEnabled = true;
            this.CountyCombo.Location = new System.Drawing.Point(157, 257);
            this.CountyCombo.Margin = new System.Windows.Forms.Padding(4);
            this.CountyCombo.Name = "CountyCombo";
            this.CountyCombo.Size = new System.Drawing.Size(486, 24);
            this.CountyCombo.TabIndex = 7;
            this.CountyCombo.SelectedIndexChanged += new System.EventHandler(this.CountyCombo_SelectedIndexChanged);
            // 
            // labelCounty
            // 
            this.labelCounty.AutoSize = true;
            this.labelCounty.Location = new System.Drawing.Point(98, 257);
            this.labelCounty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCounty.Name = "labelCounty";
            this.labelCounty.Size = new System.Drawing.Size(52, 17);
            this.labelCounty.TabIndex = 6;
            this.labelCounty.Text = "County";
            // 
            // StateCombo
            // 
            this.StateCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateCombo.FormattingEnabled = true;
            this.StateCombo.Items.AddRange(new object[] {
            "AK",
            "AL",
            "AR",
            "AZ",
            "CA",
            "CO",
            "CT",
            "DC",
            "DE",
            "FL",
            "GA",
            "HI",
            "IA",
            "ID",
            "IL",
            "IN",
            "KS",
            "KY",
            "LA",
            "MA",
            "MD",
            "ME",
            "MI",
            "MN",
            "MO",
            "MS",
            "MT",
            "NC",
            "ND",
            "NE",
            "NH",
            "NJ",
            "NM",
            "NV",
            "NY",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VA",
            "VT",
            "WA",
            "WI",
            "WV",
            "WY"});
            this.StateCombo.Location = new System.Drawing.Point(158, 208);
            this.StateCombo.Margin = new System.Windows.Forms.Padding(4);
            this.StateCombo.Name = "StateCombo";
            this.StateCombo.Size = new System.Drawing.Size(486, 24);
            this.StateCombo.TabIndex = 5;
            this.StateCombo.SelectedIndexChanged += new System.EventHandler(this.StateCombo_SelectedIndexChanged);
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(109, 211);
            this.labelState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(41, 17);
            this.labelState.TabIndex = 4;
            this.labelState.Text = "State";
            // 
            // TownCombo
            // 
            this.TownCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TownCombo.Enabled = false;
            this.TownCombo.FormattingEnabled = true;
            this.TownCombo.Location = new System.Drawing.Point(157, 309);
            this.TownCombo.Margin = new System.Windows.Forms.Padding(4);
            this.TownCombo.Name = "TownCombo";
            this.TownCombo.Size = new System.Drawing.Size(486, 24);
            this.TownCombo.TabIndex = 3;
            // 
            // labelTown
            // 
            this.labelTown.AutoSize = true;
            this.labelTown.Location = new System.Drawing.Point(107, 312);
            this.labelTown.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTown.Name = "labelTown";
            this.labelTown.Size = new System.Drawing.Size(42, 17);
            this.labelTown.TabIndex = 2;
            this.labelTown.Text = "Town";
            // 
            // OrgTypeCombo
            // 
            this.OrgTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OrgTypeCombo.FormattingEnabled = true;
            this.OrgTypeCombo.Location = new System.Drawing.Point(158, 113);
            this.OrgTypeCombo.Margin = new System.Windows.Forms.Padding(4);
            this.OrgTypeCombo.Name = "OrgTypeCombo";
            this.OrgTypeCombo.Size = new System.Drawing.Size(485, 24);
            this.OrgTypeCombo.TabIndex = 1;
            // 
            // labelOrgType
            // 
            this.labelOrgType.AutoSize = true;
            this.labelOrgType.Location = new System.Drawing.Point(25, 117);
            this.labelOrgType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOrgType.Name = "labelOrgType";
            this.labelOrgType.Size = new System.Drawing.Size(125, 17);
            this.labelOrgType.TabIndex = 0;
            this.labelOrgType.Text = "Organization Type";
            // 
            // labelInstructions
            // 
            this.labelInstructions.AutoSize = true;
            this.labelInstructions.Location = new System.Drawing.Point(8, 27);
            this.labelInstructions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(233, 153);
            this.labelInstructions.TabIndex = 0;
            this.labelInstructions.Text = resources.GetString("labelInstructions.Text");
            // 
            // groupBoxInstructions
            // 
            this.groupBoxInstructions.Controls.Add(this.labelInstructions);
            this.groupBoxInstructions.Location = new System.Drawing.Point(687, 52);
            this.groupBoxInstructions.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxInstructions.Name = "groupBoxInstructions";
            this.groupBoxInstructions.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxInstructions.Size = new System.Drawing.Size(253, 346);
            this.groupBoxInstructions.TabIndex = 14;
            this.groupBoxInstructions.TabStop = false;
            this.groupBoxInstructions.Text = "Instructions";
            // 
            // QuitButton
            // 
            this.QuitButton.Location = new System.Drawing.Point(687, 405);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.Size = new System.Drawing.Size(253, 42);
            this.QuitButton.TabIndex = 18;
            this.QuitButton.Text = "Quit";
            this.QuitButton.UseVisualStyleBackColor = true;
            this.QuitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // BGSearch
            // 
            this.BGSearch.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGSearch_DoWork);
            this.BGSearch.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGSearch_RunWorkerCompleted);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 836);
            this.Controls.Add(this.QuitButton);
            this.Controls.Add(this.OutputTable);
            this.Controls.Add(this.labelTitleEmergencyServices);
            this.Controls.Add(this.groupBoxSearch);
            this.Controls.Add(this.groupBoxInstructions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SearchForm";
            this.Load += new System.EventHandler(this.SearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OutputTable)).EndInit();
            this.groupBoxSearch.ResumeLayout(false);
            this.groupBoxSearch.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxInstructions.ResumeLayout(false);
            this.groupBoxInstructions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView OutputTable;
        private System.Windows.Forms.Label labelTitleEmergencyServices;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.TextBox textBoxOrgId;
        private System.Windows.Forms.GroupBox groupBoxSearch;
        private System.Windows.Forms.Label labelOrgId;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox OrgNameText;
        private System.Windows.Forms.Label labelOrgName;
        private System.Windows.Forms.ComboBox CountyCombo;
        private System.Windows.Forms.Label labelCounty;
        private System.Windows.Forms.ComboBox StateCombo;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.ComboBox TownCombo;
        private System.Windows.Forms.Label labelTown;
        private System.Windows.Forms.Label labelOrgType;
        private System.Windows.Forms.Label labelInstructions;
        private System.Windows.Forms.GroupBox groupBoxInstructions;
        private System.Windows.Forms.Button QuitButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox OrgTypeCombo;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button SaveButton;
        private System.ComponentModel.BackgroundWorker BGSearch;
    }
}