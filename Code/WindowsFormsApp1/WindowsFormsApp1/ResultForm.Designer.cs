﻿namespace Emergency
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.GeneralWebsiteTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.GeneralServiceAreaTextBox = new System.Windows.Forms.TextBox();
            this.GeneralCallCountTextBox = new System.Windows.Forms.TextBox();
            this.GeneralMemberCountTextBox = new System.Windows.Forms.TextBox();
            this.GeneralDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.GeneralEmailTextBox = new System.Windows.Forms.TextBox();
            this.GeneralNameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LocationTab = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.BrowserMap = new System.Windows.Forms.WebBrowser();
            this.LocationDataGrid = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.TreatmentTab = new System.Windows.Forms.TabPage();
            this.TreatmentsDataGrid = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.TrainingTab = new System.Windows.Forms.TabPage();
            this.TrainingDataGrid = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.EquipmentTab = new System.Windows.Forms.TabPage();
            this.EquipmentDataGrid = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.PhysiciansTab = new System.Windows.Forms.TabPage();
            this.PhysiciansDataGrid = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.PeopleTab = new System.Windows.Forms.TabPage();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.PeopleDataGrid = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.SiteDataGrid = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            this.LocationTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationDataGrid)).BeginInit();
            this.TreatmentTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreatmentsDataGrid)).BeginInit();
            this.TrainingTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainingDataGrid)).BeginInit();
            this.EquipmentTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentDataGrid)).BeginInit();
            this.PhysiciansTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhysiciansDataGrid)).BeginInit();
            this.PeopleTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PeopleDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.GeneralTab);
            this.tabControl1.Controls.Add(this.LocationTab);
            this.tabControl1.Controls.Add(this.TreatmentTab);
            this.tabControl1.Controls.Add(this.TrainingTab);
            this.tabControl1.Controls.Add(this.EquipmentTab);
            this.tabControl1.Controls.Add(this.PhysiciansTab);
            this.tabControl1.Controls.Add(this.PeopleTab);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1502, 652);
            this.tabControl1.TabIndex = 0;
            // 
            // GeneralTab
            // 
            this.GeneralTab.Controls.Add(this.GeneralWebsiteTextBox);
            this.GeneralTab.Controls.Add(this.label14);
            this.GeneralTab.Controls.Add(this.GeneralServiceAreaTextBox);
            this.GeneralTab.Controls.Add(this.GeneralCallCountTextBox);
            this.GeneralTab.Controls.Add(this.GeneralMemberCountTextBox);
            this.GeneralTab.Controls.Add(this.GeneralDescriptionTextBox);
            this.GeneralTab.Controls.Add(this.GeneralEmailTextBox);
            this.GeneralTab.Controls.Add(this.GeneralNameTextBox);
            this.GeneralTab.Controls.Add(this.label7);
            this.GeneralTab.Controls.Add(this.label6);
            this.GeneralTab.Controls.Add(this.label5);
            this.GeneralTab.Controls.Add(this.label4);
            this.GeneralTab.Controls.Add(this.label3);
            this.GeneralTab.Controls.Add(this.label2);
            this.GeneralTab.Controls.Add(this.label1);
            this.GeneralTab.Location = new System.Drawing.Point(4, 25);
            this.GeneralTab.Name = "GeneralTab";
            this.GeneralTab.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTab.Size = new System.Drawing.Size(1494, 623);
            this.GeneralTab.TabIndex = 0;
            this.GeneralTab.Text = "General";
            this.GeneralTab.UseVisualStyleBackColor = true;
            // 
            // GeneralWebsiteTextBox
            // 
            this.GeneralWebsiteTextBox.Enabled = false;
            this.GeneralWebsiteTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralWebsiteTextBox.Location = new System.Drawing.Point(365, 194);
            this.GeneralWebsiteTextBox.Name = "GeneralWebsiteTextBox";
            this.GeneralWebsiteTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralWebsiteTextBox.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(201, 188);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(153, 38);
            this.label14.TabIndex = 13;
            this.label14.Text = "Website: ";
            // 
            // GeneralServiceAreaTextBox
            // 
            this.GeneralServiceAreaTextBox.Enabled = false;
            this.GeneralServiceAreaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralServiceAreaTextBox.Location = new System.Drawing.Point(365, 427);
            this.GeneralServiceAreaTextBox.Name = "GeneralServiceAreaTextBox";
            this.GeneralServiceAreaTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralServiceAreaTextBox.TabIndex = 12;
            // 
            // GeneralCallCountTextBox
            // 
            this.GeneralCallCountTextBox.Enabled = false;
            this.GeneralCallCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralCallCountTextBox.Location = new System.Drawing.Point(365, 364);
            this.GeneralCallCountTextBox.Name = "GeneralCallCountTextBox";
            this.GeneralCallCountTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralCallCountTextBox.TabIndex = 11;
            // 
            // GeneralMemberCountTextBox
            // 
            this.GeneralMemberCountTextBox.Enabled = false;
            this.GeneralMemberCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralMemberCountTextBox.Location = new System.Drawing.Point(365, 302);
            this.GeneralMemberCountTextBox.Name = "GeneralMemberCountTextBox";
            this.GeneralMemberCountTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralMemberCountTextBox.TabIndex = 10;
            // 
            // GeneralDescriptionTextBox
            // 
            this.GeneralDescriptionTextBox.Enabled = false;
            this.GeneralDescriptionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralDescriptionTextBox.Location = new System.Drawing.Point(365, 249);
            this.GeneralDescriptionTextBox.Name = "GeneralDescriptionTextBox";
            this.GeneralDescriptionTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralDescriptionTextBox.TabIndex = 9;
            // 
            // GeneralEmailTextBox
            // 
            this.GeneralEmailTextBox.Enabled = false;
            this.GeneralEmailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralEmailTextBox.Location = new System.Drawing.Point(365, 144);
            this.GeneralEmailTextBox.Name = "GeneralEmailTextBox";
            this.GeneralEmailTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralEmailTextBox.TabIndex = 8;
            // 
            // GeneralNameTextBox
            // 
            this.GeneralNameTextBox.Enabled = false;
            this.GeneralNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GeneralNameTextBox.Location = new System.Drawing.Point(365, 89);
            this.GeneralNameTextBox.Name = "GeneralNameTextBox";
            this.GeneralNameTextBox.Size = new System.Drawing.Size(709, 34);
            this.GeneralNameTextBox.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(133, 421);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(223, 38);
            this.label7.TabIndex = 6;
            this.label7.Text = "Service Area: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(168, 360);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 38);
            this.label6.TabIndex = 5;
            this.label6.Text = "Call Count: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(104, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(250, 38);
            this.label5.TabIndex = 4;
            this.label5.Text = "Member Count: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(157, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 38);
            this.label4.TabIndex = 3;
            this.label4.Text = "Description: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(227, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 38);
            this.label3.TabIndex = 2;
            this.label3.Text = "E-Mail: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(233, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 38);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(510, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(381, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "General Information";
            // 
            // LocationTab
            // 
            this.LocationTab.Controls.Add(this.label16);
            this.LocationTab.Controls.Add(this.label15);
            this.LocationTab.Controls.Add(this.BrowserMap);
            this.LocationTab.Controls.Add(this.LocationDataGrid);
            this.LocationTab.Controls.Add(this.label8);
            this.LocationTab.Location = new System.Drawing.Point(4, 25);
            this.LocationTab.Name = "LocationTab";
            this.LocationTab.Padding = new System.Windows.Forms.Padding(3);
            this.LocationTab.Size = new System.Drawing.Size(1494, 623);
            this.LocationTab.TabIndex = 1;
            this.LocationTab.Text = "Locations";
            this.LocationTab.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(51, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(310, 17);
            this.label16.TabIndex = 8;
            this.label16.Text = "Click on an address cell to change browser data";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Crimson;
            this.label15.Location = new System.Drawing.Point(1256, 84);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(226, 51);
            this.label15.TabIndex = 7;
            this.label15.Text = "Note: if the browser on the bottom \r\ndisplays an error,\r\nclear your Internet Expl" +
    "orer Cache";
            // 
            // BrowserMap
            // 
            this.BrowserMap.IsWebBrowserContextMenuEnabled = false;
            this.BrowserMap.Location = new System.Drawing.Point(31, 185);
            this.BrowserMap.Margin = new System.Windows.Forms.Padding(4);
            this.BrowserMap.MinimumSize = new System.Drawing.Size(27, 25);
            this.BrowserMap.Name = "BrowserMap";
            this.BrowserMap.Size = new System.Drawing.Size(1451, 415);
            this.BrowserMap.TabIndex = 6;
            this.BrowserMap.WebBrowserShortcutsEnabled = false;
            // 
            // LocationDataGrid
            // 
            this.LocationDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.LocationDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.LocationDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LocationDataGrid.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.LocationDataGrid.Location = new System.Drawing.Point(31, 69);
            this.LocationDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.LocationDataGrid.Name = "LocationDataGrid";
            this.LocationDataGrid.ReadOnly = true;
            this.LocationDataGrid.Size = new System.Drawing.Size(1218, 97);
            this.LocationDataGrid.TabIndex = 5;
            this.LocationDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LocationDataGrid_CellContentClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(394, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(198, 48);
            this.label8.TabIndex = 1;
            this.label8.Text = "Locations";
            // 
            // TreatmentTab
            // 
            this.TreatmentTab.Controls.Add(this.TreatmentsDataGrid);
            this.TreatmentTab.Controls.Add(this.label9);
            this.TreatmentTab.Location = new System.Drawing.Point(4, 25);
            this.TreatmentTab.Name = "TreatmentTab";
            this.TreatmentTab.Padding = new System.Windows.Forms.Padding(3);
            this.TreatmentTab.Size = new System.Drawing.Size(1494, 623);
            this.TreatmentTab.TabIndex = 2;
            this.TreatmentTab.Text = "Treatments";
            this.TreatmentTab.UseVisualStyleBackColor = true;
            // 
            // TreatmentsDataGrid
            // 
            this.TreatmentsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TreatmentsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TreatmentsDataGrid.Location = new System.Drawing.Point(9, 58);
            this.TreatmentsDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.TreatmentsDataGrid.Name = "TreatmentsDataGrid";
            this.TreatmentsDataGrid.ReadOnly = true;
            this.TreatmentsDataGrid.Size = new System.Drawing.Size(1478, 561);
            this.TreatmentsDataGrid.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(539, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(230, 48);
            this.label9.TabIndex = 2;
            this.label9.Text = "Treatments";
            // 
            // TrainingTab
            // 
            this.TrainingTab.Controls.Add(this.TrainingDataGrid);
            this.TrainingTab.Controls.Add(this.label10);
            this.TrainingTab.Location = new System.Drawing.Point(4, 25);
            this.TrainingTab.Name = "TrainingTab";
            this.TrainingTab.Padding = new System.Windows.Forms.Padding(3);
            this.TrainingTab.Size = new System.Drawing.Size(1494, 623);
            this.TrainingTab.TabIndex = 3;
            this.TrainingTab.Text = "Training";
            this.TrainingTab.UseVisualStyleBackColor = true;
            // 
            // TrainingDataGrid
            // 
            this.TrainingDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TrainingDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TrainingDataGrid.Location = new System.Drawing.Point(9, 70);
            this.TrainingDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.TrainingDataGrid.Name = "TrainingDataGrid";
            this.TrainingDataGrid.ReadOnly = true;
            this.TrainingDataGrid.Size = new System.Drawing.Size(1478, 546);
            this.TrainingDataGrid.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(679, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 48);
            this.label10.TabIndex = 3;
            this.label10.Text = "Training";
            // 
            // EquipmentTab
            // 
            this.EquipmentTab.Controls.Add(this.EquipmentDataGrid);
            this.EquipmentTab.Controls.Add(this.label11);
            this.EquipmentTab.Location = new System.Drawing.Point(4, 25);
            this.EquipmentTab.Name = "EquipmentTab";
            this.EquipmentTab.Padding = new System.Windows.Forms.Padding(3);
            this.EquipmentTab.Size = new System.Drawing.Size(1494, 623);
            this.EquipmentTab.TabIndex = 4;
            this.EquipmentTab.Text = "Equipment";
            this.EquipmentTab.UseVisualStyleBackColor = true;
            // 
            // EquipmentDataGrid
            // 
            this.EquipmentDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.EquipmentDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EquipmentDataGrid.Location = new System.Drawing.Point(8, 56);
            this.EquipmentDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.EquipmentDataGrid.Name = "EquipmentDataGrid";
            this.EquipmentDataGrid.ReadOnly = true;
            this.EquipmentDataGrid.Size = new System.Drawing.Size(1479, 560);
            this.EquipmentDataGrid.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(640, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(215, 48);
            this.label11.TabIndex = 5;
            this.label11.Text = "Equipment";
            // 
            // PhysiciansTab
            // 
            this.PhysiciansTab.Controls.Add(this.PhysiciansDataGrid);
            this.PhysiciansTab.Controls.Add(this.label12);
            this.PhysiciansTab.Location = new System.Drawing.Point(4, 25);
            this.PhysiciansTab.Name = "PhysiciansTab";
            this.PhysiciansTab.Padding = new System.Windows.Forms.Padding(3);
            this.PhysiciansTab.Size = new System.Drawing.Size(1494, 623);
            this.PhysiciansTab.TabIndex = 5;
            this.PhysiciansTab.Text = "Physicians";
            this.PhysiciansTab.UseVisualStyleBackColor = true;
            // 
            // PhysiciansDataGrid
            // 
            this.PhysiciansDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PhysiciansDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PhysiciansDataGrid.Location = new System.Drawing.Point(8, 73);
            this.PhysiciansDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.PhysiciansDataGrid.Name = "PhysiciansDataGrid";
            this.PhysiciansDataGrid.ReadOnly = true;
            this.PhysiciansDataGrid.Size = new System.Drawing.Size(1479, 543);
            this.PhysiciansDataGrid.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(651, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(216, 48);
            this.label12.TabIndex = 7;
            this.label12.Text = "Physicians";
            // 
            // PeopleTab
            // 
            this.PeopleTab.Controls.Add(this.label18);
            this.PeopleTab.Controls.Add(this.label17);
            this.PeopleTab.Controls.Add(this.SiteDataGrid);
            this.PeopleTab.Controls.Add(this.SiteLabel);
            this.PeopleTab.Controls.Add(this.PeopleDataGrid);
            this.PeopleTab.Controls.Add(this.label13);
            this.PeopleTab.Location = new System.Drawing.Point(4, 25);
            this.PeopleTab.Name = "PeopleTab";
            this.PeopleTab.Padding = new System.Windows.Forms.Padding(3);
            this.PeopleTab.Size = new System.Drawing.Size(1494, 623);
            this.PeopleTab.TabIndex = 6;
            this.PeopleTab.Text = "People";
            this.PeopleTab.UseVisualStyleBackColor = true;
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(29, 67);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(39, 17);
            this.SiteLabel.TabIndex = 12;
            this.SiteLabel.Text = "Sites";
            // 
            // PeopleDataGrid
            // 
            this.PeopleDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PeopleDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.PeopleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PeopleDataGrid.Location = new System.Drawing.Point(32, 328);
            this.PeopleDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.PeopleDataGrid.Name = "PeopleDataGrid";
            this.PeopleDataGrid.ReadOnly = true;
            this.PeopleDataGrid.Size = new System.Drawing.Size(1455, 253);
            this.PeopleDataGrid.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Roboto", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(658, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(147, 48);
            this.label13.TabIndex = 9;
            this.label13.Text = "People";
            // 
            // SiteDataGrid
            // 
            this.SiteDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SiteDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.SiteDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SiteDataGrid.Location = new System.Drawing.Point(32, 89);
            this.SiteDataGrid.Name = "SiteDataGrid";
            this.SiteDataGrid.ReadOnly = true;
            this.SiteDataGrid.RowTemplate.Height = 24;
            this.SiteDataGrid.Size = new System.Drawing.Size(808, 150);
            this.SiteDataGrid.TabIndex = 13;
            this.SiteDataGrid.SelectionChanged += new System.EventHandler(this.SiteDataGrid_SelectionChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(29, 290);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 17);
            this.label17.TabIndex = 14;
            this.label17.Text = "People";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Crimson;
            this.label18.Location = new System.Drawing.Point(896, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(394, 85);
            this.label18.TabIndex = 15;
            this.label18.Text = "How to use:\r\n\r\nSelect the row whose site you want to see at the \"Sites\" table.\r\n\r" +
    "\nThe \"People\" table will generate based on your choice.\r\n";
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1499, 662);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResultForm";
            this.Text = "ResultForm";
            this.Load += new System.EventHandler(this.ResultForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.GeneralTab.ResumeLayout(false);
            this.GeneralTab.PerformLayout();
            this.LocationTab.ResumeLayout(false);
            this.LocationTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationDataGrid)).EndInit();
            this.TreatmentTab.ResumeLayout(false);
            this.TreatmentTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreatmentsDataGrid)).EndInit();
            this.TrainingTab.ResumeLayout(false);
            this.TrainingTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainingDataGrid)).EndInit();
            this.EquipmentTab.ResumeLayout(false);
            this.EquipmentTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentDataGrid)).EndInit();
            this.PhysiciansTab.ResumeLayout(false);
            this.PhysiciansTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PhysiciansDataGrid)).EndInit();
            this.PeopleTab.ResumeLayout(false);
            this.PeopleTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PeopleDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage GeneralTab;
        private System.Windows.Forms.TabPage LocationTab;
        private System.Windows.Forms.TabPage TreatmentTab;
        private System.Windows.Forms.TabPage TrainingTab;
        private System.Windows.Forms.TabPage EquipmentTab;
        private System.Windows.Forms.TabPage PhysiciansTab;
        private System.Windows.Forms.TabPage PeopleTab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox GeneralNameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox GeneralServiceAreaTextBox;
        private System.Windows.Forms.TextBox GeneralCallCountTextBox;
        private System.Windows.Forms.TextBox GeneralMemberCountTextBox;
        private System.Windows.Forms.TextBox GeneralDescriptionTextBox;
        private System.Windows.Forms.TextBox GeneralEmailTextBox;
        private System.Windows.Forms.WebBrowser BrowserMap;
        private System.Windows.Forms.DataGridView LocationDataGrid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView TreatmentsDataGrid;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView TrainingDataGrid;
        private System.Windows.Forms.DataGridView EquipmentDataGrid;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView PhysiciansDataGrid;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView PeopleDataGrid;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox GeneralWebsiteTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.DataGridView SiteDataGrid;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}