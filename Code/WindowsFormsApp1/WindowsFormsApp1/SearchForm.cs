﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Emergency
{

    /// <summary>
    /// Searches the data from an external XML link
    /// </summary>
    public partial class SearchForm : Form
    {

        private RestfulClient client = RestfulClient.Instance;
        private static string SAVE_FILENAME = "data.xml";
        private static string DIALOG_FILTER = "XML File | *.xml";
        private BackgroundWorker bgSearch = null;

        /// <summary>
        /// Basic constructor that initializes form components.
        /// </summary>
        public SearchForm()
        {
            InitializeComponent();

            bgSearch = new BackgroundWorker();
            bgSearch.DoWork += BGSearch_DoWork;
            bgSearch.RunWorkerCompleted += BGSearch_RunWorkerCompleted;
            bgSearch.WorkerReportsProgress = true;
        }

        /// <summary>
        /// Fills the organization type combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchForm_Load(object sender, EventArgs e)
        {
            client.FillOrgTypeCombo(OrgTypeCombo);
        }

        /// <summary>
        /// Resets County and Town Combo boxes and fills them with appropriate data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StateCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CountyCombo.SelectedIndex = -1;
            CountyCombo.Text = "Select County";

            TownCombo.SelectedIndex = -1;
            TownCombo.Text = "Select Town";

            client.FillTownCombo(TownCombo, (string)CountyCombo.SelectedItem, (string)StateCombo.SelectedItem, true);
            client.FillCountyCombo(CountyCombo, (string)StateCombo.SelectedItem);
        }

        /// <summary>
        /// Resets the town combo box and fills it with appropriate data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CountyCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            TownCombo.SelectedIndex = -1;
            TownCombo.Text = "Select Town";
            client.FillTownCombo(TownCombo, (string)CountyCombo.SelectedItem, (string)StateCombo.SelectedItem, false);
        }

        /// <summary>
        /// Changes a cursor and runs the search as a background action.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (bgSearch.IsBusy != true)
            {
                // Start the asynchronous operation.
                Cursor.Current = Cursors.WaitCursor;
                bgSearch.RunWorkerAsync();
            }


        }

        /// <summary>
        /// If you press it, it resets all the values you've input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, EventArgs e)
        {

            OutputTable.DataSource = null;
            OutputTable.Rows.Clear();

            OrgTypeCombo.SelectedIndex = -1;
            OrgTypeCombo.Text = "Select Organization Type";

            OrgNameText.Text = "";

            TownCombo.Items.Clear();
            TownCombo.SelectedIndex = -1;
            TownCombo.Text = "Select Town";
            TownCombo.Enabled = false;

            CountyCombo.Items.Clear();
            CountyCombo.SelectedIndex = -1;
            CountyCombo.Text = "Select County";
            CountyCombo.Enabled = false;

            StateCombo.SelectedIndex = -1;
            StateCombo.Text = "Select State";

        }

        /// <summary>
        /// Shows a result form based on the ID you typed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayButton_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse((string)textBoxOrgId.Text, out int orgId))
                client.SetOrgId(orgId);

            ResultForm resForm = new ResultForm();
            resForm.Show();
        }

        /// <summary>
        /// On click, set the Organization ID and open a Result form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OutputTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int orgId = 0;
            try
            {
                if (Int32.TryParse((string)OutputTable[0, e.RowIndex].Value, out orgId))
                {
                    client.SetOrgId(orgId);
                }
            }
       
            catch (Exception exception)
            {
                MessageBox.Show("No!!!! Click on a cell that contains result data.");
                Console.WriteLine(exception.ToString());
            }

            ResultForm resForm = new ResultForm();
            resForm.Show();
        }

        /// <summary>
        /// Closes the program if clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// On click, uses an XmlLoader's functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = DIALOG_FILTER
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = Path.GetFullPath(openFileDialog.FileName);
                MessageBox.Show("Search data successfully loaded from " + path);
                XmlLoader.LoadData(OrgTypeCombo, OrgNameText, StateCombo, CountyCombo, TownCombo, path);
                
            }

            


            
        }

        /// <summary>
        /// Uses the StoreData object to save the input values using an XmlSaver's SaveData method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (OrgTypeCombo.Text == "" && OrgNameText.Text == "" && StateCombo.Text == "" && CountyCombo.Text == "" && TownCombo.Text == "")
                {
                    MessageBox.Show("You can't save empty Values");
                }
                else
                {
                    StoreData data = new StoreData
                    {
                        OrgType = (string)OrgTypeCombo.SelectedItem,
                        OrgName = OrgNameText.Text,
                        State = (string)StateCombo.SelectedItem,
                        County = (string)CountyCombo.SelectedItem,
                        Town = (string)TownCombo.SelectedItem
                    };

                    SaveFileDialog saveFileDialog = new SaveFileDialog
                    {
                        Filter = DIALOG_FILTER,
                        Title = "Save your search",
                        FileName = "data",
                        RestoreDirectory = true
                    };

                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string path = Path.GetFullPath(saveFileDialog.FileName);
                        XmlSaver.SaveData(data, path);
                        MessageBox.Show("File was saved to " + path);
                        System.Diagnostics.Process.Start("notepad.exe",path);
                    }

                    

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Opens the file based on the default file name.
        /// </summary>
        public void OpenXmlFile()
        {
            Process myProcess = new Process();
            myProcess.StartInfo.FileName = SAVE_FILENAME;
            myProcess.Start();

        }

        /// <summary>
        /// Performs a search operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BGSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Invoke(new Action(() =>
                {
                    
                    OutputTable.DataSource = client.GetTable((string)OrgTypeCombo.SelectedItem,
                                                              OrgNameText.Text,
                                                              (string)TownCombo.SelectedItem,
                                                              (string)StateCombo.SelectedItem,
                                                              (string)CountyCombo.SelectedItem);

                    if (OutputTable.Rows.Count == 0)
                        MessageBox.Show("Data unavailable!");

                    else if((string)OrgTypeCombo.SelectedItem != "Physician")
                    {
                        OutputTable.Columns["OrganizationId"].Visible = false;
                    }

                    OutputTable.AllowUserToAddRows = false;

                    if(OutputTable.Rows.Count > 0)
                        OutputTable.Rows.RemoveAt(OutputTable.Rows.Count - 1);

                    OutputTable.ReadOnly = true;
                }));
                
            }

            catch (Exception ie)
            {
                MessageBox.Show("" + ie);
            }

        }

        /// <summary>
        /// When the search is over, it returns the result count in a message box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BGSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("The search returned " + (OutputTable.RowCount) + " results.");
        }
    }
}
