﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace Emergency
{
    /// <summary>
    /// Class for loading the xml File and printing it onto a form
    /// </summary>
    public class XmlLoader
    {
        /// <summary>
        /// Loads the data from the xml file and fills the form elements with it.
        /// </summary>
        /// <param name="orgType"></param>
        /// <param name="orgName"></param>
        /// <param name="state"></param>
        /// <param name="county"></param>
        /// <param name="town"></param>
        public static void LoadData(ComboBox orgType, TextBox orgName, ComboBox state, ComboBox county, ComboBox town, string filename)
        {
            if (File.Exists(filename))
            {
                XmlSerializer xs = new XmlSerializer(typeof(StoreData));
                FileStream read = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                StoreData data = (StoreData)xs.Deserialize(read);

                orgType.SelectedIndex = orgType.FindStringExact(data.OrgType);
                orgName.Text = data.OrgName;
                state.SelectedIndex = state.FindStringExact(data.State);
                county.SelectedIndex = county.FindStringExact(data.County);
                town.SelectedIndex = town.FindStringExact(data.Town);

                read.Flush();
                read.Close();
            }

            else
            {
                MessageBox.Show("Saved search data not found!");
            }
        }
    }
}
