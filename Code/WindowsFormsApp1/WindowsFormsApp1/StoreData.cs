﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency
{
    /// <summary>
    /// Stores all the data of the search form 
    /// </summary>
    public class StoreData
    {
        private string orgType, orgName, state, county, town;

        /// <summary>
        /// Getters and setters for OrgType
        /// </summary>
        public string OrgType
        {
            get { return orgType;  }
            set { orgType = value; }
        }

        /// <summary>
        /// Getters and Setter for OrgName
        /// </summary>
        public string OrgName
        {
            get { return orgName; }
            set { orgName = value; }
        }

        /// <summary>
        /// Getters and setters for State
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        /// <summary>
        /// Getters and Setters for County
        /// </summary>
        public string County
        {
            get { return county; }
            set { county = value; }
        }

        /// <summary>
        /// Getters and setters for Town
        /// </summary>
        public string Town
        {
            get { return town; }
            set { town = value; }
        }

    }
}
