﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emergency
{
    /// <summary>
    /// Form that displays results based on the search data.
    /// </summary>
    public partial class ResultForm : Form
    {
        private RestfulClient client = RestfulClient.Instance;

        /// <summary>
        /// Default Constructor that initializes form components
        /// </summary>
        public ResultForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads all of the objects according to a certain ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultForm_Load(Object sender, EventArgs e)
        {
            client.GetTabs(tabControl1);

            client.GetGeneral(GeneralNameTextBox, GeneralEmailTextBox, GeneralWebsiteTextBox, GeneralDescriptionTextBox, GeneralMemberCountTextBox, GeneralCallCountTextBox, GeneralServiceAreaTextBox);
            LocationDataGrid.DataSource = client.GetLocations(BrowserMap, 1);
            TreatmentsDataGrid.DataSource = client.GetTreatment();
            TrainingDataGrid.DataSource = client.GetTraining();
            EquipmentDataGrid.DataSource = client.GetEquipment();
            PhysiciansDataGrid.DataSource = client.GetPhysicians();

            if (client.GetSiteForPeople() != null)
            {
                SiteDataGrid.DataSource = client.GetSiteForPeople();
                SiteDataGrid.Columns[0].Visible = false;
                SiteDataGrid.Columns[2].Visible = false;
                SiteDataGrid.Columns[3].Visible = false;
            }
            
            
        
        }
         
        /// <summary>
        /// Refreshes the Location tab based on the location you chose
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            client.GetLocations(BrowserMap, e.RowIndex);
        }
        
        /// <summary>
        /// Fills the people table based on what you select for in the Site table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SiteDataGrid_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                string siteId = SiteDataGrid.Rows[SiteDataGrid.CurrentCell.RowIndex].Cells[2].Value.ToString();
                PeopleDataGrid.DataSource = client.GetPeople(siteId);
                PeopleDataGrid.Columns[0].Visible = false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
        }
    }
}
