﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Emergency
{
    /// <summary>
    /// Communicates with an external XML file and parses its data.
    /// </summary>
    class RestfulClient
    {
        private static RestfulClient inst;
        private string homeUri = "http://simon.ist.rit.edu:8080/Services/resources/ESD/";
        private HttpWebRequest htRequest;
        private HttpWebResponse htResponse;
        private XmlReader xmlR;
        private int orgId;
        private string mapURL;
        private DataTable siteTable;

        /// <summary>
        /// Default empty constructor
        /// </summary>
        public RestfulClient()
        {

        }
        /// <summary>
        /// Creates an instance of the RestfulClient
        /// </summary>
        public static RestfulClient Instance
        {
            get
            {
                if (inst == null)
                    inst = new RestfulClient();

                return inst;
            }
        }
        
        /// <summary>
        /// Sets the Organization ID
        /// </summary>
        /// <param name="orgId"></param>
        public void SetOrgId(int orgId)
        {
            this.orgId = orgId;
        }

        /// <summary>
        /// Fills the Org type combo box with appropriate data
        /// </summary>
        /// <param name="box"></param>
        public void FillOrgTypeCombo(ComboBox box)
        {
            string newURI = homeUri + "OrgTypes";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                while (xmlR.ReadToFollowing("type"))
                {
                    xmlR.Read();
                    if (xmlR.Value != "")
                        box.Items.Add(xmlR.Value);
                }

                htResponse.Close();

            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }

        /// <summary>
        /// Fills the Town combo box with approriate data
        /// </summary>
        /// <param name="box"></param>
        /// <param name="countyName"></param>
        /// <param name="stateName"></param>
        /// <param name="showAll"></param>
        public void FillTownCombo(ComboBox box, string countyName, string stateName, bool showAll)
        {
            box.Items.Clear();
            string newURI = "";
            if (!showAll)
                newURI = homeUri + "Cities?county=" + countyName;
            else
                newURI = homeUri + "Cities?state=" + stateName;

            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                while (xmlR.ReadToFollowing("city"))
                {
                    xmlR.Read();
                    if (xmlR.Value != "")
                        box.Items.Add(xmlR.Value);
                }

                box.Enabled = true;
                htResponse.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Fills the County combo box with approriate data
        /// </summary>
        /// <param name="box"></param>
        /// <param name="stateName"></param>
        public void FillCountyCombo(ComboBox box, string stateName)
        {
            box.Items.Clear();

            string newURI = homeUri + "Counties?state=" + stateName;
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                while (xmlR.ReadToFollowing("CountyName"))
                {
                    xmlR.Read();
                    if (xmlR.Value != "")
                        box.Items.Add(xmlR.Value);
                }

                box.Enabled = true;
                htResponse.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }

        /// <summary>
        /// Creates the output table based on search criteria
        /// </summary>
        /// <param name="orgType"></param>
        /// <param name="orgName"></param>
        /// <param name="townName"></param>
        /// <param name="stateName"></param>
        /// <param name="countyName"></param>
        /// <returns></returns>
        public DataTable GetTable(string orgType, string orgName, string townName, string stateName, string countyName)
        {
            string newURI = homeUri + "Organizations";
            string[] credentials = { "?type=", "&name=", "&town=", "&state=", "&county=" };
            string[] values = { orgType, orgName, townName, stateName, countyName };

            for (int i = 0; i < credentials.Length; i++)
            {
                newURI += credentials[i];
                if (credentials[i] != null)
                    newURI += values[i];
            }

            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;


            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);

                htResponse.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            DataTable dataTable = myData.Tables["row"];
            
            if (orgType == "Physician")
            {
                dataTable = new DataView(dataTable).ToTable(false, "fName", "mName" , "lName");
            }

            return dataTable;
        }

        /// <summary>
        /// Gets and sets up the tabs
        /// </summary>
        /// <param name="contr"></param>
        public void GetTabs(TabControl contr)
        {
            htRequest = (HttpWebRequest)WebRequest.Create(homeUri + "Application/Tabs?orgId=" + orgId);
            htRequest.Method = "GET";

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                string[] tabCollection = { "General", "Locations", "Treatment", "Training", "Equipment", "Physicians", "People" };
                string[] resultTabs = new string[7];

                for (int i = 0; i < tabCollection.Length; i++)
                {
                    xmlR.ReadToFollowing("Tab");
                    xmlR.Read();
                    string tabName = xmlR.Value.ToString();
                    resultTabs[i] = tabName;
                }

                //remove all empty tabs
                string[] noTabs = tabCollection.Except(resultTabs).ToArray();

                foreach (string value in noTabs)
                    contr.TabPages.RemoveByKey(value + "Tab");

                htResponse.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Fills the General Info tab with appropriate data
        /// </summary>
        /// <param name="text1"></param>
        /// <param name="text2"></param>
        /// <param name="text3"></param>
        /// <param name="text4"></param>
        /// <param name="text5"></param>
        /// <param name="text6"></param>
        /// <param name="text7"></param>
        public void GetGeneral(TextBox text1, TextBox text2, TextBox text3, TextBox text4, TextBox text5, TextBox text6, TextBox text7)
        {
            TextBox[] texts = { text1, text2, text3, text4, text5, text6, text7 };
            string[] reads = { "name", "description", "email", "website", "nummembers", "numcalls", "servicearea" };

            string newURI = homeUri + orgId + "/General";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                for (int i = 0; i < texts.Length; i++)
                {
                    string readData = reads[i];
                    xmlR.ReadToFollowing(readData);
                    xmlR.Read();
                    texts[i].Text = xmlR.Value.ToString();
                    if (IsNullByAnyOtherName(texts[i]))
                    {
                        texts[i].Text = "No available data";
                    }
                }

                htResponse.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }

        /// <summary>
        /// Tests if the textbox is empty in every possible way
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool IsNullByAnyOtherName(TextBox text)
        {
            bool result = false;
            if (text.Text == null || text.Text.Equals("null") || text.Text.Equals("") || text.Text.Length == 0)
                result = true;
            return result;

        }

        /// <summary>
        /// Gets the location data and builds a map
        /// </summary>
        /// <param name="brow"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public DataTable GetLocations(WebBrowser brow, int index)
        {
            string newURI = homeUri + orgId + "/Locations";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                                
                mapURL  = "http://maps.google.com/maps?q="
                        + myData.Tables[1].Rows[index][1] + ",+"
                        + myData.Tables[1].Rows[index][3] + ",+"
                        + myData.Tables[1].Rows[index][4] + ",+";

                brow.Navigate(mapURL);
                htResponse.Close();

                DataTable myTable = myData.Tables["location"];

                DataView view = new DataView(myTable);
                DataTable viewTable = view.ToTable(false, "type", "address1", "address2", "city", "state", "zip", "phone", "ttyPhone", "fax", "latitude", "longitude", "countyName");

                return viewTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }


        }

        /// <summary>
        /// Fills the Treatment tab with appropriate data
        /// </summary>
        /// <returns></returns>
        public DataTable GetTreatment()
        {
            string newURI = homeUri + orgId + "/Treatments";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();

                DataTable myTable = myData.Tables["treatment"];

                DataView view = new DataView(myTable);
                DataTable viewTable = view.ToTable(false, "type", "abbreviation");

                return viewTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }



            
        }

        /// <summary>
        /// Fills the Training tab with appropriate data
        /// </summary>
        /// <returns></returns>
        public DataTable GetTraining()
        {
            string newURI = homeUri + orgId + "/Training";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();
                DataTable myTable = myData.Tables["training"];

                DataView view = new DataView(myTable);
                DataTable viewTable = view.ToTable(false, "type", "abbreviation");

                return viewTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }


        }

        /// <summary>
        /// Fills the Equipment tab with appropriate data
        /// </summary>
        /// <returns></returns>
        public DataTable GetEquipment()
        {
            string newURI = homeUri + orgId + "/Equipment";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();
                
                DataTable myTable = myData.Tables["equipment"];

                DataView view = new DataView(myTable);
                DataTable viewTable = view.ToTable(false, "type", "quantity", "description");

                return viewTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }

        }

        /// <summary>
        /// Fills the Physicians tab with appropriate data
        /// </summary>
        /// <returns></returns>
        public DataTable GetPhysicians()
        {
            string newURI = homeUri + orgId + "/Physicians";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();
                DataTable myTable = myData.Tables["physician"];

                DataView view = new DataView(myTable);
                DataTable viewTable = view.ToTable(false, "fName", "mName", "lName", "suffix", "phone", "license");

                return viewTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

        public DataTable GetSiteForPeople()
        {
            string newURI = homeUri + orgId + "/People";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;
           

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();
                DataTable myTable = myData.Tables["site"];

                return myTable;

                
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Fills the People tab with appropriate data
        /// </summary>
        /// <returns></returns>
        public DataTable GetPeople(string siteId)
        {
            string newURI = homeUri + orgId + "/People";
            htRequest = (HttpWebRequest)WebRequest.Create(newURI);
            htRequest.Method = "GET";
            DataSet myData = null;

            try
            {
                htResponse = (HttpWebResponse)htRequest.GetResponse();
                Stream stream = htResponse.GetResponseStream();
                xmlR = XmlReader.Create(stream);

                myData = new DataSet();
                myData.ReadXml(xmlR);
                htResponse.Close();
                siteTable = myData.Tables["site"];
                DataTable personTable = myData.Tables["person"];
                
                for(int i = 0; i< siteTable.Columns.Count; i++)
                    Console.WriteLine(siteTable.Columns[i]);
                Console.WriteLine();
                for (int i = 0; i < personTable.Columns.Count; i++)
                    Console.WriteLine(personTable.Columns[i]);

                for (int i = 0; i < personTable.Rows.Count - 1; i++)
                {
                    if (!personTable.Rows[i]["site_id"].ToString().Trim().ToUpper().Contains(siteId))
                        personTable.Rows[i].Delete();
                }
                
                return personTable;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

    }
} 