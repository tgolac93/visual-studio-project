﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

/// <summary>
/// Class for saving the data into xml
/// </summary>
namespace Emergency
{
    public class XmlSaver
    {
        /// <summary>
        /// Saves the data into an xml 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="filename"></param>
        public static void SaveData(Object o, string filename)
        {
            XmlSerializer sr = new XmlSerializer(o.GetType());
            TextWriter wr = new StreamWriter(filename);
            sr.Serialize(wr, o);
            wr.Close();
        }
    }
}
